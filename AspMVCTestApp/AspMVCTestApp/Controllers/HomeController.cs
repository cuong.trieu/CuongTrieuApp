﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AspMVCTestApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AspMVCTestApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            CustomerEditViewModel model = new CustomerEditViewModel()
            {
                CustomerID = "113",
                CustomerName = "Police 113",
                SelectedCountryIso3 = "SelectedCountryIso3",
                SelectedRegionCode = "SelectedRegionCode",
                Regions = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "Regions",
                        Value = "Regions"
                    }
                },
                Countries = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "Countries",
                        Value = "Countries"
                    }
                },
            };
            return View(model);
        }
        //[HttpPost]
        //public IActionResult Index()
        //{
            
        //    return View();
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
