﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseClassEvents.MyEvent;

namespace BaseClassEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle cir = new Circle(100);

            cir.Update(5);
            Console.ReadLine();
        }
        // Special EventArgs class to hold info about Shapes.
      

        public class Circle : Shape
        {
            private double radius;
            public Circle(double d)
            {
                radius = d;
                area = 3.14 * radius * radius;
            }
            public void Update(double d)
            {
                radius = d;
                area = 3.14 * radius * radius;
                List<string> list = new List<string> { "1", "2", "3" };
                OnShapeChanged(new ShapeEventArgs(area, list));
            }
            protected override void OnShapeChanged(ShapeEventArgs e)
            {
                // Do any circle-specific processing here.

                // Call the base class event invocation method.
                base.OnShapeChanged(e);
            }
            public override void Draw()
            {
                Console.WriteLine("Drawing a circle");
            }
        }
    }
}
