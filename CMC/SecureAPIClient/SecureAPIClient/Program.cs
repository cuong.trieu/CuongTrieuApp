using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureAPIClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();

            //AuthConfig config = AuthConfig.ReadFromJsonFile("appsettings.json");
            //Console.WriteLine($"Authority: {config.Authority}");
            RunAsync().GetAwaiter().GetResult();
        }

        private static async Task RunAsync()
        {
            AuthConfig config = AuthConfig.ReadFromJsonFile("appsettings.json");

            IConfidentialClientApplication app;

            app = ConfidentialClientApplicationBuilder.Create(config.ClientId)
                .WithClientSecret(config.ClientSecret)
                .WithAuthority(new Uri(config.Authority))
                .Build();

            string[] ResourceIds = new string[] { config.ResourceID };

            AuthenticationResult result = null;
            try
            {
                result = await app.AcquireTokenForClient(ResourceIds).ExecuteAsync();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Token acquired \n");
                Console.WriteLine(result.AccessToken);
                Console.ResetColor();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        //public static IHostBuilder CreateHostBuilder(string[] args) =>
        //    Host.CreateDefaultBuilder(args)
        //        .ConfigureWebHostDefaults(webBuilder =>
        //        {
        //            webBuilder.UseStartup<Startup>();
        //        });

        public static class ApplicationBuilderExtensions
        {
            public static IApplicationBuilder UseAzureADBearerAuthentication(
                this IApplicationBuilder app,
                IConfigurationRoot configuration)
            {
                var tenant = configuration.GetSection("AzureAD:Tenant").Value;
                var azureADInstance = configuration.GetSection("AzureAD:AzureADInstance").Value;
                var audience = configuration.GetSection("AzureAD:Audience").Value;
                var authority = $"{azureADInstance}{tenant}";


                JwtBearerOptions jwtBearerAuthOptions = new JwtBearerOptions
                {
                    Audience = audience,
                    //AutomaticAuthenticate = true,
                    //AutomaticChallenge = true,
                    Authority = authority
                };
                app.UseJwtBearerAuthentication(jwtBearerAuthOptions);
                return app;
            }
        }
    }
}
