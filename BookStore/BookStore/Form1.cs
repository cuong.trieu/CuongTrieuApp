﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class frmMaintainBook : Form
    {
        // Declare object to working with data
        BookManager bm = new BookManager();
        // Declare object contain data
        DataTable dtBook;
        public frmMaintainBook()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
           
        }

        private void b(object sender, EventArgs e)
        {
           
        }
        private void getAllBooks() {
            dtBook = bm.getBooks();
            txtBookID.DataBindings.Clear();
            txtBookTitle.DataBindings.Clear();
            txtBookPrice.DataBindings.Clear();
            txtBookQuantity.DataBindings.Clear();
            txtBookID.DataBindings.Add("Text", dtBook, "BookID");
            txtBookTitle.DataBindings.Add("Text", dtBook, "BookTitle");
            txtBookPrice.DataBindings.Add("Text", dtBook, "BookPrice");
            txtBookQuantity.DataBindings.Add("Text", dtBook, "BookQuantity");
            dgvBookList.DataSource = dtBook;
            lbTotalQuantity.Text = "TotalQuantity: " + dtBook.Compute("SUM(BookQuantity)", string.Empty);

        }
        private void btnLoad_Click(object sender, EventArgs e)
        {
            getAllBooks();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtBookTitle_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMaintainBook_Load(object sender, EventArgs e)
        {

        }
    }
}
