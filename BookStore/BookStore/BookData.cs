﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace BookStore
{
    class BookData
    {
    }
    public class BookManager {
        string strConnection;
        public BookManager() {
            strConnection = getConnectionString();
        }
        // Get connectionString method
        public string getConnectionString() {
            string strConnection = "Data Source=VIETCUONGPC;Initial Catalog=Manager;Integrated Security=SSPI;";
            return strConnection;
        }
        // Get all book method
        public DataTable getBooks() {
            string SQL = "select * from Books";
            SqlConnection cnn = new SqlConnection(strConnection);
            SqlCommand cmd = new SqlCommand(SQL,cnn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dtBook = new DataTable();
            try {
                if (cnn.State == ConnectionState.Closed)
                {
                    cnn.Open();
                }
                da.Fill(dtBook);
            }
            catch (SqlException se) {
                throw new Exception(se.Message);
            }

            finally { cnn.Close(); }
            return dtBook;
        }
        public bool addNewBook(Book book) {
            SqlConnection cnn = new SqlConnection(strConnection);
            string SQL = "Insert Books values(@ID,@Title,@Quantity,@Price)";
            SqlCommand cmd = new SqlCommand(SQL, cnn);
            cmd.Parameters.AddWithValue("@ID", book.BookID);
            cmd.Parameters.AddWithValue("@Title", book.BookTitle);
            cmd.Parameters.AddWithValue("@Price", book.BookPrice);
            cmd.Parameters.AddWithValue("@Quantity", book.BookQuantity);
            if (cnn.State == ConnectionState.Closed)
            {
                cnn.Open();
            }
            int count = cmd.ExecuteNonQuery();
            return (count > 0);
        }
        public bool deleteBook(int BookID) {
            SqlConnection cnn = new SqlConnection(strConnection);
            string SQL = "Delete Books where BookID=@ID";
            SqlCommand cmd = new SqlCommand(SQL, cnn);
            cmd.Parameters.AddWithValue("@ID", BookID);
            if (cnn.State == ConnectionState.Closed)
            {
                cnn.Open();
            }
            int count = cmd.ExecuteNonQuery();
            return (count > 0);
        }
    }
}
