﻿namespace AirportManage
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAirports = new System.Windows.Forms.Button();
            this.btnPlans = new System.Windows.Forms.Button();
            this.btnHelicopters = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(253, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "AIR PORT MANAGEMENT";
            // 
            // btnAirports
            // 
            this.btnAirports.Location = new System.Drawing.Point(198, 123);
            this.btnAirports.Name = "btnAirports";
            this.btnAirports.Size = new System.Drawing.Size(267, 23);
            this.btnAirports.TabIndex = 1;
            this.btnAirports.Text = "AirPorts";
            this.btnAirports.UseVisualStyleBackColor = true;
            this.btnAirports.Click += new System.EventHandler(this.btnAirports_Click);
            // 
            // btnPlans
            // 
            this.btnPlans.Location = new System.Drawing.Point(198, 173);
            this.btnPlans.Name = "btnPlans";
            this.btnPlans.Size = new System.Drawing.Size(267, 23);
            this.btnPlans.TabIndex = 1;
            this.btnPlans.Text = "Plans";
            this.btnPlans.UseVisualStyleBackColor = true;
            // 
            // btnHelicopters
            // 
            this.btnHelicopters.Location = new System.Drawing.Point(198, 227);
            this.btnHelicopters.Name = "btnHelicopters";
            this.btnHelicopters.Size = new System.Drawing.Size(267, 23);
            this.btnHelicopters.TabIndex = 1;
            this.btnHelicopters.Text = "Helicopters";
            this.btnHelicopters.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 329);
            this.Controls.Add(this.btnHelicopters);
            this.Controls.Add(this.btnPlans);
            this.Controls.Add(this.btnAirports);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAirports;
        private System.Windows.Forms.Button btnPlans;
        private System.Windows.Forms.Button btnHelicopters;
    }
}