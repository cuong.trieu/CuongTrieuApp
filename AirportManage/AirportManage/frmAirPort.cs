﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AirportManage
{
    public partial class frmAirPort : Form
    {
        public frmAirPort()
        {
            InitializeComponent();
        }
        public AirPort Air = new AirPort();
        DataTable dt = new DataTable();
        private void frmAirPort_Load(object sender, EventArgs e)
        {
            try
            {
                // your code here 
                string CSVFilePathName = @"AirPort.csv";
                string[] Lines = File.ReadAllLines(CSVFilePathName);
                string[] Fields;
                Fields = Lines[0].Split(new char[] { ';' });
                int Cols = Fields.GetLength(0);
                //1st row must be column names; force lower case to ensure matching later on.
                for (int i = 0; i < Cols; i++)
                    dt.Columns.Add(Fields[i].ToLower(), typeof(string));
                DataRow Row;
                for (int i = 1; i < Lines.GetLength(0); i++)
                {
                    Fields = Lines[i].Split(new char[] { ';' });
                    Row = dt.NewRow();
                    for (int f = 0; f < Cols; f++)
                        Row[f] = Fields[f];
                    dt.Rows.Add(Row);
                }
                dgvAirPort.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error is " + ex.ToString());
                throw;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int index = dgvAirPort.Rows.Count - 2;
            DataRow Row;
            Row = dt.NewRow();
            string Id = dgvAirPort.Rows[index].Cells[0].Value.ToString();
            Row[0] = Id;
            string Name = dgvAirPort.Rows[index].Cells[1].Value.ToString();
            Row[0] = Name;
            string Runwaysize = dgvAirPort.Rows[index].Cells[2].Value.ToString();
            Row[0] = Runwaysize;
            string Maxfixedwingpakingplace = dgvAirPort.Rows[index].Cells[3].Value.ToString();
            Row[0] = Maxfixedwingpakingplace;
            dt.Rows.Add(Air.Idsb, Air.Name, Air.Minneededrunwaysize, Air.Maxfixedwingparkingplace);
            dgvAirPort.DataSource = dt;
        }

        private void dgvAirPort_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                dgvAirPort.Rows[e.RowIndex].ErrorText =
                    "Must not be empty";
                e.Cancel = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(@"AirPort.csv");
            int iColCount = dt.Columns.Count;
            sw.Write(sw.NewLine);
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt.Columns[i].ToString());
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator);
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
        }
    }
}
