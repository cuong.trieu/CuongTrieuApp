﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportManage
{
    public class AirPort
    {
        private string _idsb;
        public string Idsb
        {
            get { return _idsb; }
            set { _idsb = value; }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _minneededrunwaysize;
        public string Minneededrunwaysize
        {
            get { return _minneededrunwaysize; }
            set { _minneededrunwaysize = value; }
        }
        private string _maxfixedwingparkingplace;
        public string Maxfixedwingparkingplace
        {
            get { return _maxfixedwingparkingplace; }
            set { _maxfixedwingparkingplace = value; }
        }
    }
}
