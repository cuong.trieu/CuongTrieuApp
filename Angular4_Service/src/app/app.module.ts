import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HighlightDirective222 } from './highlight.directive';
import { FormSubmitDirective } from './form.submit.directive';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppChildComponent } from './app.child';
import { MyserviceService } from './myservice.service';
import { HttpClientModule } from '@angular/common/http';
import { SignupRformComponent } from './formValidation/signup-rform.component';

@NgModule({
  imports: [ BrowserModule,
    FormsModule,HttpClientModule,
    ReactiveFormsModule  ],
  declarations: [
    AppComponent,
    HighlightDirective222,
    FormSubmitDirective,
    AppChildComponent,
    SignupRformComponent
  ],
  providers: [MyserviceService],
  bootstrap: [ SignupRformComponent ]
})
export class AppModule { }
