import { Component, HostListener, Input, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, Products } from './Dto/Product';

@Component({
  selector: 'app-child-root',
  // template: ``,
  templateUrl: './app.child.html'
  // inputs: ['appchildroot']
})

export class AppChildComponent {
  color: string;
  constructor() { }
  //Recive from parent
  @Input("inputName") avatarUrl: string = 'Default1';
  clickEvent() {
    console.log(this.avatarUrl)
  }
  @Output() voteSize = new EventEmitter();
  @Output() ProductEvent = new EventEmitter();
  @Output() avtTextEvent = new EventEmitter();
  counter: number = 0;
  Products: Product[] = [
    { id: 1, nameproduct: 'Iphone X' },
    { id: 2, nameproduct: 'Samsung S9' },
    { id: 3, nameproduct: 'Sony Xperia XZs' },
  ];
  //Trigger and send data to parrent
  voted() {
    this.counter++;
    this.voteSize.emit(this.counter);
    this.ProductEvent.emit(Products);
    // Hàm vote sẽ tăng counter lên 1, đồng thời thông qua EventEmitter bắn value counter này ra component cha
  }
  //Trigger change on childrent
  onchangeAvaterText() {
    this.avtTextEvent.emit(this.avatarUrl)
  }
}
