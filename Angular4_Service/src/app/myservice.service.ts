import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
 
    apiURL: string = 'https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22';
    constructor(private httpClient: HttpClient) {}
 
  getTodayDate() {
    let ndate = new Date();
    // return this.httpClient.get<any[]>("https://community-open-weather-map.p.rapidapi.com/weather");
    return ndate;
  }
}