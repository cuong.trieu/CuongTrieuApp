import { Component, HostListener, Input, EventEmitter, Output, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'tpc-signup-rform',
  templateUrl: './signup-rform.component.html'
})
export class SignupRformComponent implements OnInit, AfterViewInit {
  form: FormGroup;
  constructor(private fb: FormBuilder) { }
  isDisable = true;
  ngOnInit() {
    this.form = this.fb.group({
      custome: ['', [Validators.required]],
      username: ['', [Validators.required]],
      pw: this.fb.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      })
    });
  }
  ngAfterViewInit() {
    this.onChanges();
  }
  //Listening for change in form control`
  onChanges(): void {
    this.form.valueChanges.subscribe(val => {
      if (val.custome.includes("Yes") || val.custome.includes("yes")) {
        this.isDisable = false;
      }
      else this.isDisable = true;
    });
  }

  onSubmit() {
    console.log(this.form);
    alert("I am look handsome, ohh that is destination :):):):)")
  }
}