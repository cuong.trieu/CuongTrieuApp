import { Component, HostListener, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MyserviceService } from './myservice.service';
import { AppChildComponent } from './app.child';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements AfterViewInit {
  color: string;
  test: Number;
  active: boolean;
  public foods;
  todaydate;
  constructor(private myservice: MyserviceService) { }
  ngAfterViewInit() {
    console.log("Affter ne!");
    console.log(this.AppChildComponent);
  }
  //Using ViewChild to get all childrent varible
  @ViewChild(AppChildComponent, {static: true}) AppChildComponent: ElementRef;
  ngOnInit() {
    //Test call a service
    this.todaydate = this.myservice.getTodayDate();
    console.log(this.todaydate);
    //Using ViewChild to get all childrent varible
    console.log(this.AppChildComponent);
  }
  @HostListener('click', ['$event'])
  onClick(e) {
    console.log();
  }
  mynumer: string = "C:\Works";
  isUnchanged: boolean = false;
  clickEvent() {
    console.log('FormSubmitDirective')
  }
  vote: any;
  product: any;
  voteCount(value) {
    this.vote = value;
    console.log(this.AppChildComponent)
  }
  ProductEvent(value) {
    this.product = value;
    console.log(this.product)
  }
  onchangeAvaterText(value){
    this.mynumer = value;
    // console.log(this.mynumer)
  }
  BreakingNews() {
    // var data = null;

    // var xhr = new XMLHttpRequest();
    // xhr.withCredentials = true;

    // xhr.addEventListener("readystatechange", function () {
    //   if (this.readyState === this.DONE) {
    //     console.log(this.responseText);
    //   }
    // });

    // xhr.open("GET", "https://myallies-breaking-news-v1.p.rapidapi.com/GetTopNews");
    // xhr.setRequestHeader("x-rapidapi-host", "myallies-breaking-news-v1.p.rapidapi.com");
    // xhr.setRequestHeader("x-rapidapi-key", "1e9e3edf69msh256435e7477b807p17406ajsnad69d5899469");

    // xhr.send(data);
    //npm config set https-proxy http://tce1hc:munmun2019@rb-proxy-de.bosch.com:8080
  }
}
