import { Directive, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appsubmit]',
})
export class FormSubmitDirective {
  constructor() { }

  @Input('appsubmit') appsubmit: string = "default";

  @HostListener('click')
  click() {
    // this.appsubmit = "ProductEvent triggered!";
    console.log(this.appsubmit);
  }
}
