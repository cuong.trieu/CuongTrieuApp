﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DynamicParam
{
    class Program
    {
        public static void Main(string[] args)
        {
            if (Compare2String(3, 3))
            {
                Console.WriteLine("The two hash values are the same");
            }
            else
                Console.WriteLine("The two hash values are not the same");

            Console.ReadLine();
        }
        public static bool Compare2String(dynamic string1, dynamic string2)
        {
            var HashString1 = HashString(Convert.ToString(string1));
            var HashString2 = HashString(Convert.ToString(string2));
            bool bEqual = false;
            if (HashString1.Length == HashString2.Length)
            {
                int i = 0;
                while ((i < HashString1.Length) && (HashString1[i] == HashString2[i]))
                {
                    i += 1;
                }
                if (i == HashString1.Length)
                {
                    bEqual = true;
                }
            }
            return bEqual;

        }
        public static byte[] HashString(dynamic sSourceData)
        {
            byte[] tmpSource;
            byte[] tmpHash;
            //Create a byte array from source data.
            tmpSource = ASCIIEncoding.ASCII.GetBytes(sSourceData);
            //Compute hash based on source data.
            tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);
            return tmpHash;
            //Console.WriteLine(ByteArrayToString(tmpHash));
        }
        static string ByteArrayToString(byte[] arrInput)
        {
            int i;
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (i = 0; i < arrInput.Length; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }
    }
}
