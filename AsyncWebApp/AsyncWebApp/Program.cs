﻿using System;
using System.Threading.Tasks;

namespace AsyncWebApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = Task<int>.Factory.StartNew(() => {
                // Just loop.
                int max = 1000000;
                int ctr = 0;
                for (ctr = 0; ctr <= max; ctr++)
                {
                    if (ctr == max / 2 && DateTime.Now.Hour <= 12)
                    {
                        ctr++;
                        break;
                    }
                }
                return ctr;
            });
            Console.WriteLine("Hello World!");
           
            Console.WriteLine("Finished {0:N0} iterations.", t.Result);
        }
    }
}
