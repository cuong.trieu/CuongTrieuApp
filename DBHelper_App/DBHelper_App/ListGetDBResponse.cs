﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DBHelper_App
{
    static class ListGetDBResponse
    {
        //public static IEnumerable<DataTable> GetTestCases()
        //{

        //    IEnumerable<GetDBResponse> tc = new List<GetDBResponse>()
        //    {
        //       new GetDBResponse(){
        //        GID = "1",
        //        LOCALID = "tceh1c",
        //        PID = 1212,
        //        SSID = "2121"
        //       },
        //       new GetDBResponse()
        //       {
        //        GID = "1",
        //        LOCALID = "tceh1c",
        //        PID = 1212,
        //        SSID = "2121"
        //       }
        //    };
        //}
        public static DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("PID", typeof(decimal));
            table.Columns.Add("GID", typeof(string));
            table.Columns.Add("LOCALID", typeof(string));
            table.Columns.Add("SSID", typeof(string));

            // Here we add five DataRows.
            table.Rows.Add(25, "Indocin", "David", DateTime.Now);
            table.Rows.Add(50, "Enebrel", "Sam", DateTime.Now);
            table.Rows.Add(10, "Hydralazine", "Christoff", DateTime.Now);
            table.Rows.Add(21, "Combivent", "Janet", DateTime.Now);
            table.Rows.Add(100, "Dilantin", "Melanie", DateTime.Now);
            return table;
        }
    }
}
