﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DBHelper_App
{
    class Program
    {
        public static string constr;
        public static void Main()
        {
            try
            {

                //OracleConnection con = new OracleConnection(constr);
                //con.Open();

                //string cmdstr = "SELECT * FROM UDB_CSP.V_BIZPARTNER where ROWNUM <= 100";
                //OracleConnection connection = new OracleConnection(constr);
                //OracleCommand cmd = new OracleCommand(cmdstr, con);

                //OracleDataReader reader = cmd.ExecuteReader();

                //var ds = new DataSet();
                //var da = new OracleDataAdapter(cmd);
                //// Declare the variables to retrieve the data in EmpInfo
                //byte[] byteCodes = new byte[10];
                //da.Fill(ds);

                //// Read the next row until end of row
                //DataTable DT = new DataTable();
                //DT = ds.Tables[0];
                //using mockup
                DataTable MockDT = new DataTable();
                MockDT = ListGetDBResponse.GetTable();

                List<GetDBResponse> dbList = new List<GetDBResponse>();
                IEnumerable<DataRow> sequence = MockDT.AsEnumerable();
                dbList = DataTableToList<GetDBResponse>(MockDT);

                // Clean up
                //reader.Dispose();
                //con.Dispose();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
            }
        }

        public static List<T> DataTableToList<T>(DataTable dt) where T : class, new()
        {
            List<T> lstItems = new List<T>();
            if (dt != null && dt.Rows.Count > 0)
                foreach (DataRow row in dt.Rows)
                    lstItems.Add(ConvertDataRowToGenericType<T>(row));
            else
                lstItems = null;
            return lstItems;
        }

       
        public object GetParameterValue()
        {
            return "null";
        }

        private static T ConvertDataRowToGenericType<T>(DataRow row) where T : class, new()
        {
            Type entityType = typeof(T);
            T objEntity = new T();
            foreach (DataColumn column in row.Table.Columns)
            {
                object value = row[column.ColumnName];
                if (value == DBNull.Value) value = null;
                PropertyInfo property = entityType.GetProperty(column.ColumnName, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
                try
                {
                    if (property != null && property.CanWrite)
                        property.SetValue(objEntity, value, null);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return objEntity;
        }

        public static object GetParameterValue(string parameterName)
        {
            object acb = new object();
            return acb;
        }
    }
}
