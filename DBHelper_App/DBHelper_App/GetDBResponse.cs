﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBHelper_App
{
    
    class GetDBResponse
    {
        public decimal? PID { get; set; }
        public string? GID { get; set; }
        public string? LOCALID { get; set; }
        public string? SSID { get; set; }
    }
}
