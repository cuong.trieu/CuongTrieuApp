﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// TODO:https://www.c-sharpcorner.com/UploadFile/naresh.avari/develop-and-install-a-windows-service-in-C-Sharp/
        /// Using visual studio administrator: developer  command Promt for VS...
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                Service1 service = new Service1();
                service.RunAsConsole(args);
                Trace.WriteLine("dev");
            }
            else
            {
                ServiceBase[] ServicesToRun;
                Trace.WriteLine("Q");
                ServicesToRun = new ServiceBase[]
                {
                new Service1()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
