﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SM.App.Spa.TaskScheduler
{
    public class GracePeriodManagerService : BackgroundService
    {
        private readonly ILogger<GracePeriodManagerService> _logger;
        private readonly IConfiguration _config;
        private IServiceScopeFactory _serviceScopeFactory { get; set; }

        public GracePeriodManagerService(ILogger<GracePeriodManagerService> logger, IServiceScopeFactory serviceScopeFactory, IConfiguration config)
        {
            //Constructor’s parameters validations...
            
            //_flexiService = flexiService;
            _serviceScopeFactory = serviceScopeFactory;
            _config = config;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug($"GracePeriodManagerService is starting.");
            //_flexiDbContext.Database.ExecuteSqlCommand("EXEC SM.ReleaseExpiredFlexiWsAllocated GO");
           

            stoppingToken.Register(() =>
                _logger.LogDebug($"GracePeriod background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogDebug($"GracePeriod task doing background work.");
                //_flexiDbContext.Database.ExecuteSqlCommand("EXEC SM.ReleaseExpiredFlexiWsAllocated GO");
                // This eShopOnContainers method is querying a database table
                // and publishing events into the Event Bus (RabbitMQ / ServiceBus)
                CheckConfirmedGracePeriodOrders();

                await Task.Delay(1, stoppingToken);
            }

            _logger.LogDebug($"GracePeriod background task is stopping.");
        }

        private void CheckConfirmedGracePeriodOrders()
        {
            //using (var scope = _serviceScopeFactory.CreateScope())
            //{
            //    var context = scope.ServiceProvider.GetRequiredService<FlexiDbContext>();
            //    context.Database.ExecuteSqlCommand("EXEC SM.ReleaseExpiredFlexiWsAllocated");
            //    // now do your work
            //}
            //_flexiUserService.GetAssociate("TCE1HC");
            //_flexiDbContext.Database.ExecuteSqlCommand("EXEC SM.ReleaseExpiredFlexiWsAllocated GO");
            _logger.LogDebug($"GracePeriod task doing background work.");
            Console.WriteLine(_config["ConnectionStrings:CustomConnection"]);
            Console.WriteLine("Scheduler trigger each : " + DateTime.UtcNow);
            //Console.ReadLine();
        }
    }
}
