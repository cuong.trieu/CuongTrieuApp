﻿using System;
using System.Threading.Tasks;
using Mansion.Core.Services;
using Mansion.Core.Helpers;

namespace ConsomeAPI
{
    class Program
    {
        public static HttpDataService HttpDataService = new HttpDataService();
        static void Main(string[] args)
        {
            GetApi();
        }
        public static async Task<string> GetApi()
        {
            var RESULT = await Json.StringifyAsync(HttpDataService.GetAsync<string>("https://localhost:5001/api/CallApi/GetCurrentUniversalTime", accessToken: null, forceRefresh: true));
            return RESULT;
        }
    }
}
