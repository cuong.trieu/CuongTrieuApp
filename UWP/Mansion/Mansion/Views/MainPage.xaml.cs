﻿using System;

using Mansion.ViewModels;
using Windows.Globalization;
using Windows.UI.Xaml.Controls;

namespace Mansion.Views
{
    public sealed partial class MainPage : Page
    {
        private MainViewModel ViewModel => DataContext as MainViewModel;

        public MainPage()
        {
            InitializeComponent();
        }
    }
}
