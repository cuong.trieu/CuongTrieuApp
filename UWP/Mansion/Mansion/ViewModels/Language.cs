﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mansion.ViewModels
{
    public class Language
    {
        public string DisplayName { get; set; }
        public string LanguageCode { get; set; }
    }
}
