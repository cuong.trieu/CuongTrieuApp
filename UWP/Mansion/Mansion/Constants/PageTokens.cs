﻿namespace Mansion
{
    internal static class PageTokens
    {
        public const string MainPage = "Main";
        public const string SettingsPage = "Settings";
    }
}
