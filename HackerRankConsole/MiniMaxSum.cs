﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class MiniMaxSums
    {
        /*
        * Complete the 'miniMaxSum' function below.
        *
        * The function accepts INTEGER_ARRAY arr as parameter.
        */

        public static void miniMaxSum(List<int> arr)
        {
            try
            {
                if (arr.Where(x => x < 1).Any() || arr.Where(x => x > Math.Pow(10, 9)).Any() || arr.Count < 4)
                {
                    Console.WriteLine("0 0");
                    return;
                }
                long result1 = 0;
                long result2 = 0;
                long max = arr.Max();
                long min = arr.Min();
                for (int i = 0; i < arr.Count; i++)
                {
                    result1 += arr[i];
                    result2 += arr[i];
                }
                Console.WriteLine("{0} {1}", result1 - max, result2 - min);
            }
            catch (Exception)
            {
                Console.WriteLine("0 0");
                return;
            }
        }
    }
}
