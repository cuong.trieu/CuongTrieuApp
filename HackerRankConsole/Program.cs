﻿using ImmortalConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankConsole
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Console.WriteLine("todaidot");
            //Process process = new Process();
            //FindIndex();
            //MathAbs();
            //int[][] array = new int[3][] { new int[3] { 12, 2, 4 }, new int[3] { 4, 5, 6 }, new int[3] { 10, 8, -12 } };
            //List<List<int>> arr = new List<List<int>>();
            //arr.Add(new List<int>() { 12, 2, 4 });
            //arr.Add(new List<int>() { 4, 5, 6 });
            //arr.Add(new List<int>() { 10, 8, -12 });
            //var result = diagonalDifference(arr);
            //QueueProcess();
            //encryption("haveaniceday");

            //int n = Convert.ToInt32(Console.ReadLine().Trim());
            //ExtraLongFactorials.extraLongFactorials(n);
            List<int> arr = new List<int>();
            arr.Add(6);
            arr.Add(-4);
            arr.Add(3);
            arr.Add(-9);
            arr.Add(0);
            arr.Add(4);
            arr.Add(1);
            PlusMinuss.plusMinus(arr);
        }
        /*
             * Complete the 'climbingLeaderboard' function below.
             *
             * The function is expected to return an INTEGER_ARRAY.
             * The function accepts following parameters:
             *  1. INTEGER_ARRAY ranked
             *  2. INTEGER_ARRAY player
        */
        public static List<int> reverseArray(List<int> a)
        {
            a.Reverse();
            return a;
        }

        public static List<int> climbingLeaderboard(List<int> ranked, List<int> player)
        {
            List<int> listRankResult = new List<int>();
            Dictionary<int, int> leaderBoard = new Dictionary<int, int>();
            int dynamicRanked = 1;
            for (int i = 0; i < ranked.Count(); i++)
            {
                if (i == 0)
                {
                    leaderBoard.Add(ranked[i], dynamicRanked);
                }
                else if (ranked[i] == ranked[i] - 1)
                {
                    leaderBoard.Add(ranked[i], dynamicRanked);
                }
                else
                {
                    dynamicRanked++;
                    leaderBoard.Add(ranked[i], dynamicRanked);
                }
            }
            do
            {
                int firstPlayerItem = player.First();
                for (int i = 0; i < ranked.Count(); i++)
                {
                    if (firstPlayerItem > ranked[i])
                    {
                    }
                }
            } while (player.Count() > 0);
            return null;

        }

        // Complete the encryption function below.
        static string encryption(string s)
        {
            int length = s.Trim().Length;
            double sqrt = Math.Sqrt(length);
            int afterSqrt = Convert.ToInt32(Math.Round(sqrt, MidpointRounding.AwayFromZero));
            List<string> listSource = new List<string>();
            List<string> listAlterSource = new List<string>();
            int count = 0;
            string item = "";
            foreach (char c in s)
            {
                item = item + c.ToString();
                ++count;
                if (count == afterSqrt)
                {
                    listAlterSource.Add(item);
                    item = "";
                    count = 0;
                }
            }
            string finnalResult = "";
            foreach (var child in listAlterSource)
            {
                finnalResult += child + " ";
            }
            finnalResult.Trim();
            return finnalResult;
        }

        public static void QueueProcess()
        {
            Console.WriteLine("Enter number of item in queue:");
            int i = Convert.ToInt32(Console.ReadLine());
            List<int> listFifo = new List<int>();
            List<int> listLiFo = new List<int>();
            int count = 0;
            do
            {
                Console.Clear();
                foreach (var item in listLiFo)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("Enter item: " + count);

                string strItem = Console.ReadLine();
                try
                {
                    if (strItem.Split(' ').Count() > 1)
                    {
                        listLiFo.Add(Convert.ToInt32(strItem.Split(' ')[1]));
                        i--;
                        count++;
                    }
                    else if (Convert.ToInt32(strItem) == 2)
                    {
                        if (listLiFo.Count() > 0)
                        {
                            listLiFo.RemoveAt(listLiFo.Count() - 1);
                        }
                    }
                    else if (Convert.ToInt32(strItem) == 3)
                    {
                        Console.WriteLine(listLiFo[listLiFo.Count() - 1]);
                        i++;
                    }
                }
                catch
                {
                    throw;
                }

            } while (i != 0);
            {

            }
        }

        /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */
        public static int diagonalDifference(List<List<int>> arr)
        {
            int leftToRighDiagonal = 0;
            int righToLeftDiagonal = 0;
            foreach (List<int> subArray in arr)
            {
                int rowIndex = arr.IndexOf(subArray);
                leftToRighDiagonal = leftToRighDiagonal + subArray[rowIndex];
                righToLeftDiagonal = righToLeftDiagonal + subArray[subArray.Count() - rowIndex - 1];
            }
            return Math.Abs(leftToRighDiagonal - righToLeftDiagonal);
        }
        public static int MathAbs()
        {
            var result = Math.Abs(-4 + 2);
            return result;
        }
        public static void FindIndex()
        {
            List<int> list = new List<int>
            {
                1,6,2,4
            };
            foreach (var item in list)
            {
                Console.WriteLine(list.IndexOf(item));
            }
        }

        // Complete the aVeryBigSum function below.
        static long aVeryBigSum(long[] ar)
        {
            long sumResult = ar.Sum();
            return sumResult;
        }

        static int solveMeFirst(int a, int b)
        {
            return a + b;
        }

        // Complete the compareTriplets function below.
        static List<int> compareTriplets(List<int> a, List<int> b)
        {
            int aScore = 0;
            int bScore = 0;
            for (int i = 0; i < a.Count(); i++)
            {
                if (a[i] > b[i])
                {
                    aScore++;
                }
                else if (a[i] < b[i])
                {
                    bScore++;
                }
            }
            List<int> finalScore = new List<int>()
            {
                aScore,
                bScore
            };
            return finalScore;
        }
    }
}
