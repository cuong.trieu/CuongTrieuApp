﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class SimpleArraySums
    {

        /*
         * Complete the 'simpleArraySum' function below.
         *
         * The function is expected to return an INTEGER.
         * The function accepts INTEGER_ARRAY ar as parameter.
         */
        public static int SimpleArraySum(List<int> ar)
        {
            if (ar.Count < 0 || ar.Count > 1000)
            {
                return 0;
            }
            int sum = ar.Sum();
            return sum;
        }
    }
}
