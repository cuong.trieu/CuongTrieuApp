﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class BirthdayCakeCandle
    {

        /*
         * Complete the 'birthdayCakeCandles' function below.
         *
         * The function is expected to return an INTEGER.
         * The function accepts INTEGER_ARRAY candles as parameter.
         */

        public static int birthdayCakeCandles(List<int> candles)
        {
            List<int> positive = new List<int>();
            List<int> negative = new List<int>();
            List<int> zero = new List<int>();
            if (candles.Count < 1 || candles.Count > Math.Pow(10, 5))
            {
                return 0;
            }

            if (candles.Where(x => x < 1 || x > Math.Pow(10, 7)).Any())
            {
                return 0;
            }
            int maxValue = candles.Max();
            return candles.Where(x => x == maxValue).Count();
        }
    }
}
