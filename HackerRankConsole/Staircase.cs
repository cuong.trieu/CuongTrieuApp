﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class PlusMinuss
    {
        /*
         * Complete the 'plusMinus' function below.
         *
         * The function accepts INTEGER_ARRAY arr as parameter.
         */
        public static void plusMinus(List<int> arr)
        {
            List<int> positive = new List<int>();
            List<int> negative = new List<int>();
            List<int> zero = new List<int>();
            try
            {
                zero = arr.Where(x => x == 0).ToList();
                negative = arr.Where(x => x < 0).ToList();
                positive = arr.Where(x => x > 0).ToList();
                Console.WriteLine((double)positive.Count / arr.Count);
                Console.WriteLine((double)negative.Count / arr.Count);
                Console.WriteLine((double)zero.Count / arr.Count);
            }
            catch (Exception)
            {
                Console.WriteLine(0);
                Console.WriteLine(0);
                Console.WriteLine(0);
            }
        }
    }
}
