﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class ExtraLongFactorials
    {
        public static void extraLongFactorials(int n)
        {
            BigInteger result = n;
            result = Process(n, ref result);
            Console.WriteLine(result);
        }
        public static BigInteger Process(int n, ref BigInteger result)
        {
            try
            {
                var factorial = BigInteger.One;
                for (int i = 1; i <= n; i++)
                    factorial = factorial * i;
                return factorial;
            }

            catch (Exception ex)
            {
                return result;
            }
        }

    }
}
