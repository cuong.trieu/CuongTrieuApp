﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class GradingStudent
    {

        /*
         * Complete the 'gradingStudents' function below.
         *
         * The function is expected to return an INTEGER_ARRAY.
         * The function accepts INTEGER_ARRAY grades as parameter.
         */

        public static List<int> gradingStudents(List<int> grades)
        {
            try
            {
                for (int i = 0; i < grades.Count; i++)
                {
                    var roundItem = Math.Round((double)grades[i]);
                    if (roundItem < grades[i])
                    {
                        roundItem += 5;
                    }
                    if ((roundItem - grades[i]) < 3)
                    {
                        grades[i] = (int)roundItem;
                    }
                }
                return grades;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
