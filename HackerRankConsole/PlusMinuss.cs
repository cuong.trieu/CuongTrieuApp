﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class Staircases
    {
        /*
         * Complete the 'staircase' function below.
         *
         * The function accepts INTEGER n as parameter.
         */

        public static void staircase(int n)
        {
            try
            {
                if (n < 0 || n > 100)
                {
                    Console.WriteLine("#");
                }
                for (int i = 1; i <= n; i++)
                {
                    var spaces = new String(' ', n - i);
                    var hashes = new String('#', i);
                    Console.WriteLine(spaces + hashes);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("#");
            }

        }
    }
}
