﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ImmortalConsole
{
    public class TimeConversions
    {

        /*
           * Complete the 'timeConversion' function below.
           *
           * The function is expected to return a STRING.
           * The function accepts STRING s as parameter.
        */

        public static string timeConversion(string s)
        {
            try
            {
                var dateTime = DateTime.Parse(s);
                return dateTime.ToString("HH:mm:ss");
            }
            catch (Exception)
            {

                return DateTime.Now.ToString("HH:mm:ss");
            }
        }
    }
}
