import React, { Component } from 'react';
import { Header } from '../common/Header';
import { Menu } from '../common/Menu';

export class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      finalBid: props.location.state.finalBid,
      bid: window.DEFAULT_BID,
      maxBid: props.location.state.maxBid,
      history: props.location.state.history
    };
  }

  onFinalBidChange = (event) => {
    this.setState({ finalBid: event.target.value });
  }
  onBidChange = (event) => {
    this.setState({ bid: event.target.value });
  }
  onMaxBidChange = (event) => {
    this.setState({ maxBid: event.target.value });
  }
  onBidClick = () => {
    this.setState({ finalBid: parseInt(this.state.bid) + this.state.finalBid });
    this.setState({ history: this.state.history.concat({ bid: this.state.bid, time: Date().toLocaleString() }) });
  }
  render() {
    return (
      <div className='main-wrapper'>
        <Header />
        <div className='row'>
          <Menu />
          <div className='col-sm-9'>
            <div className='row'>
              <div className='col-4'>
                <div className='row'>
                  <img src={window.MOCK_PRODUCT_URL1} className='img-fluid' alt='Responsive' />
                </div>
                <div className='row'>
                  <img src={window.MOCK_PRODUCT_URL1} className='img-square' alt='Responsive' />
                </div>
              </div>
              <div className='col-6'>
                <div className='row'>
                  <div className='col-2'><i className='fas fa-chart-pie fas-left'></i></div>
                  <div className='col-10'>
                    <div className='row'> {this.props.location.state.title}</div>
                    <div className='row'> {this.props.location.state.content}</div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-8'>
                    <table className='table table-sm'>
                      <thead>
                        <tr>
                          <th scope='col'>After bid</th>
                          <th scope='col'>Bid</th>
                          <th scope='col'>Max bid</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <div className='row'>
                              <div className='col'><input className='square-no-padding' value={this.state.finalBid} onChange={this.onFinalBidChange} type='text'></input></div>
                              <div className='col'><i className='fas fa-caret-left'></i></div>
                            </div>
                            <div className='row'>
                              <div className='col'></div>
                              <div className='col'><i className='fas fa-caret-right'></i></div>
                            </div>
                          </td>
                          <td><input className='square-no-color' type='text' value={this.state.bid} onChange={this.onBidChange}></input></td>
                          <td><input className='square-no-padding' type='text' value={this.state.maxBid} onChange={this.onMaxBidChange}></input></td>
                        </tr>
                        <tr>
                          <td><button type='button' onClick={this.onBidClick} className='btn btn-secondary'>Place bid</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className='col-4'>
                    {this.state.history.map((item, i) => {
                      return (<div key={i} className='row'>
                        <div className='row'>
                          <div className='col'>Your bid</div>
                          <div className='col'>{item.time}</div>
                        </div>
                        <div className='row square-no-padding'>{item.bid}</div>
                      </div>)
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}