import React, { Component } from 'react';
import { Header } from '../common/Header';
import { Menu } from '../common/Menu';
import {  Link } from 'react-router-dom';

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: window.PRODUCTS
    };
  }
  displayName = Home.name;

  render() {
    return (
      <div className='main-wrapper'>
        <Header />
        <div className='row'>
          <Menu />
          <div className='col-sm-9'>
            <div className='row'>Products</div>
            <div className='row'>
              <div className='col-sm-6'><img className='img-fluid' alt='Responsive' src={window.MOCK_PRODUCT_URL1}></img></div>
              <div className='col'><img className='img-fluid' alt='Responsive' src={window.MOCK_PRODUCT_URL2}></img></div>
            </div>
            <div className='row'>
              <ProductElement></ProductElement>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function ProductElement() {
  const productElement =  window.PRODUCTS.map((item, index) => (
    <div className='col-sm-6' key={index}>
        <Link to={{ pathname: '/product-detail', state: item }} >
        <div className='row'>
          <div className = 'col-2'><i className='fas fa-chart-pie'></i></div>
          <div className='col'>
            <div className='row'>
              <div className='row'> {item.title}</div>
              <div className='row'> {item.time}</div>
            </div>
          </div>
        </div>
        </Link>
    </div>
  ));
  return productElement;
}