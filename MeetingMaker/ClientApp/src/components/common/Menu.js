import React, { Component } from 'react';

export class Menu extends Component {
  render () {
    return (
      <div className='col-sm-3'>
      <ul>
        <li><a href='#home'><img className='avt' alt='Responsive' src={window.DEFAULT_USER_URL}></img></a></li>
        <li><a href='#home'>{window.DEFAULT_USER_NAME}</a></li>
        <li><a href='#home'><p ><span className='square'></span>148</p></a></li>
        <li><a href='#home'><span className=''>MENU</span></a></li>
        <li><a href='#home'><i className='fas fa-shopping-cart'></i>SHOP</a></li>
        <li><a href='#home'><i className='fas fa-gavel'></i>AUCTION</a></li>
        <li><a href='#home'><i className='fas fa-sign-out-alt'></i>SIGN OUT</a></li>
      </ul>
    </div>
    );
  }
}
