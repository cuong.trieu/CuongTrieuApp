import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/products/Home';
import './custom.css'
import { ProductDetail } from './components/product-detail/ProductDetail';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/product-detail' component={ProductDetail} />
      </Layout>
    );
  }
}

