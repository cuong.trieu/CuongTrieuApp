var LOGO_URL = 'asset/logo.png';
var DEFAULT_USER_URL = 'asset/user.png';
var DEFAULT_USER_NAME = 'JONAS HAMMARHOG';
var MOCK_PRODUCT_URL1 = 'asset/teamwork-in-the-workplace.jpg';
var MOCK_PRODUCT_URL2 = 'asset/google-office.jpg';
var PRODUCTS = [
    { id: 1, title: 'Gym Card - Nordic Wellness', time: '24 days 9 hours 20 minutes 31 seconds', content: '1 year of training with Nordic Wellness. The product is worth 5990 SEK.', finalBid: 139, maxBid: 9, imageUrl: 'asset/teamwork-in-the-workplace.jpg', history: [{ bid: 9, time: '18/6-16 19:52' }, { bid: 9, time: '18/6-16 19:53' }] },
    { id: 2, title: 'Gym Card VIP - Nordic Wellness', time: '24 days 9 hours 20 minutes 31 seconds', content: '1 year of training with Nordic Wellness. The product is worth 5990 SEK.', finalBid: 129, maxBid: 10, imageUrl: 'asset/google-office.jpg', history: [{ bid: 9, time: '18/6-16 19:52' }, { bid: 9, time: '18/6-16 19:53' }] }
];
var DEFAULT_BID = 9;