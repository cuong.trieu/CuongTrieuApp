﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Delegate
            testDelC("xxxxxxxxx");
            //Example: Anonymous Func with lambda expression
            //SUM
            Func<int, int, int> Sum = (x, y) => x + y;
            Console.WriteLine("SUM: " + Sum(10, 5));
            Console.WriteLine("SUM2: " + Sum2(10, 5));
            //RANDOM
            Func<int> getRandomNumber = () => new Random().Next(1, 100);
            Console.WriteLine("getRandomNumber: " + getRandomNumber());
            //Call back delegate
            TinhTong(5, 7, Info);
            TinhTong(3, 4, Warning);
            Console.ReadLine();
        }
        public delegate void TestDelegate(string s);
        public static TestDelegate testDelC = (x) => { Console.WriteLine("Delegate: " + x); };

        public Func<int, int, int> Sum = (x, y) => x + y;
        public static Func<int, int, int> Sum2 = new Func<int, int, int>(DoProcess);
        public static int DoProcess(int x, int y)
        {
            return x + y;
        }
        #region callback delegate
        static public void Info(string s)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(string.Format("Info: {0}", s));
            Console.ResetColor();
        }

        static public void Warning(string s)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(string.Format("Waring: {0}", s));
            Console.ResetColor();
        }

        // Khai báo hàm có tham số là delegate
        static void TinhTong(int a, int b, Action<string> callback)
        {
            int c = a + b;
            // Gọi delegate
            callback("Tổng là: " + c); // Hoặc callback.Invoke("Tổng là: "  + c);
        }
        #endregion
    }
}
